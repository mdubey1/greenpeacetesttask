locals {
  timestamp = formatdate("YYMMDDhhmmss", timestamp())
  root_dir  = abspath("../src/")
}

# Compress source code
data "archive_file" "source" {
  type        = "zip"
  source_dir  = local.root_dir
  output_path = "/tmp/function-${local.timestamp}.zip"
}

# Create bucket that will host the source code
resource "google_storage_bucket" "bucket" {
  name     = "${var.project}-function"
  location = "us-central1"
}

resource "google_project_service" "enable_cloud_resource_manager_api" {
  project                    = var.project
  service                    = "cloudresourcemanager.googleapis.com"
  disable_dependent_services = true
}

# Add source code zip to bucket
resource "google_storage_bucket_object" "zip" {
  # Append file MD5 to force bucket to be recreated
  name   = "source.zip#${data.archive_file.source.output_md5}"
  bucket = google_storage_bucket.bucket.name
  source = data.archive_file.source.output_path
}

# Enable Cloud Functions API
resource "google_project_service" "cf" {
  project = var.project
  service = "cloudfunctions.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false

  depends_on = [
    google_project_service.enable_cloud_resource_manager_api
  ]
}

# Enable Cloud Build API
resource "google_project_service" "cb" {
  project = var.project
  service = "cloudbuild.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false

  depends_on = [
    google_project_service.enable_cloud_resource_manager_api
  ]
}

# Create PUB/SUB Cloud Function
resource "google_cloudfunctions_function" "pubsub" {
  name    = "pubsub_main"
  runtime = "python39" # Switch to a different runtime if needed

  depends_on = [
    google_project_service.cf
  ]

  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.zip.name
  entry_point           = "pubsub_main"

  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource   = var.pubsub_topic.id

    failure_policy {
      retry = true
    }
  }
}

# Create Http Cloud Function
resource "google_cloudfunctions_function" "http" {
  name    = "http_main"
  runtime = "python39" # Switch to a different runtime if needed

  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.zip.name
  trigger_http          = true
  entry_point           = "http_main"

  depends_on = [
    google_project_service.cf
  ]
}

# Create IAM entry so all users can invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.http.project
  region         = google_cloudfunctions_function.http.region
  cloud_function = google_cloudfunctions_function.http.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
