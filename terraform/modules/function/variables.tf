variable "project" {}
variable "function_name" {
  type    = list(string)
  default = ["pubsub_main", "http_main"]
}
variable "pubsub_topic" {}
