terraform {
  backend "gcs" {
    bucket = "testtask-terraform-state"
    prefix = "terraform/state"
  }
}

provider "google" {
  project = var.project
  region  = var.region
}

module "pubsub" {
  source  = "./modules/pubsub"
  project = var.project
}

module "function" {
  source       = "./modules/function"
  project      = var.project
  pubsub_topic = module.pubsub.pubsub_topic
}

