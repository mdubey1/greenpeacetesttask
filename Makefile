install:
	cd src && pip install -r requirements.txt

lint:
	cd src && pylint .

run dev:
	cd src && functions-framework --target=http_main --debug

tf-init:
	cd terraform && terraform init

tf-validate: tf-init
	cd terraform && terraform validate

tf-plan: tf-validate
	cd terraform && terraform plan -out tf.plan

tf-apply: tf-plan
	cd terraform && terraform apply --auto-approve tf.plan

tf-destroy: tf-init
	cd terraform && terraform destroy
