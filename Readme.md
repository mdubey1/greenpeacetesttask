# Greenpeace Test Task

This python application fetches data from third party API which contains IBM Stock Timeseries Monthly data. The application contains 2 cloud functions:-
* `pubsub_main`
* `http_main`

The `pubsub_main` function fetches data from third party API and stores it in BigQuery and Firestore cache. It is triggered by pubsub topic `ibm_monthly`.
The `http_main` function is a http endpoint which returns data for particular date. The date is passed as a query parameter.

## Setup Guide

The application uses Gitlab CI and terraform to deploy cloud functions to GCP. On push it will run the pipeline.

For local testing run the `http_main` function using functions framework:
`functions-framework --target=http_main --debug`

## Testing on the deployed functions

To trigger the `pubsub_main` function use this url to trigger pubsub topic:-
```
curl -i https://us-central1-upbeat-repeater-252607.cloudfunctions.net/trigger_pubsub
```

To get data from `http_main` use:-

```
curl -i https://us-central1-upbeat-repeater-252607.cloudfunctions.net/http_main?date=2021-11-17
```
