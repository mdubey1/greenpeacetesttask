import logging

from google.cloud import bigquery, firestore
from google.cloud.exceptions import NotFound

from constants import COLLECTION, PROJECT_ID

bigquery_client = bigquery.Client(project=PROJECT_ID)
db = firestore.Client(project=PROJECT_ID)


def insert_into_firestore(collection: str, document: str, data: dict):
    doc_ref = db.collection(collection).document(document)
    doc_ref.set(data)
    logging.info("Created document {}".format(document))


def read_from_firestore(collection: str, document: str):
    doc_ref = db.collection(collection).document(document)
    db_record = doc_ref.get()
    record = db_record.to_dict()
    return record


def check_record_in_firestore(collection: str, document: str):
    doc_ref = db.collection(collection).document(document)
    db_record = doc_ref.get()
    return db_record.exists


def bq_create_dataset(dataset_id):
    dataset_ref = bigquery_client.dataset(dataset_id)

    try:
        bigquery_client.get_dataset(dataset_ref)
    except NotFound:
        dataset = bigquery.Dataset(dataset_ref)
        dataset = bigquery_client.create_dataset(dataset)
        logging.info('Dataset {} created.'.format(dataset.dataset_id))


def bq_create_table(dataset_id, table_name):
    dataset_ref = bigquery_client.dataset(dataset_id)

    table_ref = dataset_ref.table(table_name)

    try:
        bigquery_client.get_table(table_ref)
    except NotFound:
        schema = [
            bigquery.SchemaField('date', 'STRING', mode='REQUIRED'),
            bigquery.SchemaField('open', 'FLOAT'),
            bigquery.SchemaField('high', 'FLOAT'),
            bigquery.SchemaField('low', 'FLOAT'),
            bigquery.SchemaField('close', 'FLOAT'),
            bigquery.SchemaField('volume', 'INTEGER'),
        ]
        table = bigquery.Table(table_ref, schema=schema)
        table = bigquery_client.create_table(table)
        logging.info('Table {} created.'.format(table.table_id))


def bq_insert_rows_json(dataset_id, table_name, rows):
    if not rows:
        return
    database_ref = bigquery_client.dataset(dataset_id)
    table_ref = database_ref.table(table_name)
    errors = bigquery_client.insert_rows_json(
        table_ref,
        json_rows=rows
    )

    assert errors == []

    logging.info("Created records in BQ")


def bq_read_row_by_date(project_id, dataset_id, table_name, date):
    '''
    Takes date as parameter and return the corresponding row from bigquery
    '''

    query = ('SELECT * FROM `{}.{}.{}` WHERE date="{}" LIMIT 1'.format(project_id,
             dataset_id, table_name, date))

    try:
        query_job = bigquery_client.query(query)
        result = list(query_job.result())
        if len(result) > 0:
            return dict(result[0])
        return None
    except Exception as e:
        logging.error("Error", e)
        return None


def transform_data(data):
    '''
    This method transforms data according to bigquery schema and creates a cache of data in firestore.
    It checks if data already exists in firestore, if it doesn't add it to firestore and than biqquery
    '''

    timeseries = data.get('Monthly Time Series')

    rows = []
    for value in timeseries.items():

        if check_record_in_firestore(COLLECTION, value[0]):
            logging.info("Record exists!")
            continue
        d = {}
        d['date'] = value[0]
        d['open'] = value[1].get('1. open')
        d['high'] = value[1].get('2. high')
        d['low'] = value[1].get('3. low')
        d['close'] = value[1].get('4. close')
        d['volume'] = value[1].get('5. volume')
        insert_into_firestore(COLLECTION, d['date'], d)
        rows.append(d)

    return rows
