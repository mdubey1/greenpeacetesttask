import logging

import requests

from constants import *
from utils import *

logging.basicConfig(level=logging.INFO)


def pubsub_main(event, context):

    # Check if dataset exists if not create one
    bq_create_dataset(DATASET_ID)

    # Check if table exists if not create one
    bq_create_table(DATASET_ID, TABLE_NAME)

    # Fetch data from third party api
    response = requests.get(EXTERNAL_API_ENDPOINT)
    data = response.json()

    # Transform data according to bigquery schema
    rows = transform_data(data)

    # Insert data to bigquery
    bq_insert_rows_json(DATASET_ID, TABLE_NAME, rows)


def http_main(request):

    date = request.args.get('date')

    if not date:
        return {"message": "Date is required"}, 400

    cache_record = read_from_firestore(COLLECTION, date)

    if cache_record:
        return cache_record, 200

    bq_record = bq_read_row_by_date(PROJECT_ID, DATASET_ID, TABLE_NAME, date)

    if not bq_record:
        return {}, 404

    return bq_record, 200
