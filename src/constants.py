PROJECT_ID = 'upbeat-repeater-252607'
EXTERNAL_API_ENDPOINT = "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=IBM&apikey=demo"
DATASET_ID = "ibmdata"
TABLE_NAME = "monthlytimeseries"
COLLECTION = "ibm"
